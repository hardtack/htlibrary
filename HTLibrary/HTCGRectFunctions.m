//
//  HTCGRectFunctions.m
//  HTLibrary
//
//  Created by 최건우 on 13. 9. 1..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTCGRectFunctions.h"


CGRect CGRectApplyPadding(CGRect rect, UIEdgeInsets padding) {
    rect.origin.x += padding.left;
    rect.origin.y += padding.top;
    rect.size.width -= padding.left + padding.right;
    rect.size.height -= padding.top + padding.bottom;
    return rect;
}