//
//  HTFile.h
//
//  Created by 최건우 on 13. 6. 16..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTFile : NSObject

@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString* mimeType;

- (id)initWithData:(NSData *)data name:(NSString *)name mimeType:(NSString *)mimeType;
+ (HTFile *)fileWithData:(NSData *)data name:(NSString *)name mimeType:(NSString *)mimeType;

@end
