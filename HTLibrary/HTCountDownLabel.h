//
//  HTCountDownLabel.h
//
//  Created by 최건우 on 13. 6. 28..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HTCountDownLabel;

@protocol HTCountDownLabelDelegate <NSObject>

- (void)countDownLabelDidFire:(HTCountDownLabel *)label;

@end

typedef NSString *(^HTTimeIntervalFormmaterBlock)(NSDate *currentDate, NSDate *toDate);

@interface HTCountDownLabel : UILabel

@property (weak, nonatomic) id<HTCountDownLabelDelegate> delegate;

@property (strong, nonatomic) HTTimeIntervalFormmaterBlock formatter;
@property (strong, nonatomic) NSDate *toDate;
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic) NSTimeInterval interval;

@end
