//
//  NSString+HTSizeAdditions.m
//  PinkPouch
//
//  Created by 최건우 on 13. 6. 29..
//  Copyright (c) 2013년 Rolling Square. All rights reserved.
//

#import "NSString+HTSizeAdditions.h"
#import "UIView+FrameAddition.h"

@implementation NSString (HTSizeAdditions)

- (CGSize)sizeWithLabel:(UILabel *)label {
    if (label.numberOfLines == 1) {
        return [self sizeWithFont:label.font
                         forWidth:label.frame.size.width
                    lineBreakMode:label.lineBreakMode];
    } else {
        return [self sizeWithFont:label.font
                constrainedToSize:CGSizeMake(label.frame.size.width, CGFLOAT_MAX)
                    lineBreakMode:label.lineBreakMode];
    }
}

@end
