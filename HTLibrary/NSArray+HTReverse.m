//
//  NSArray+HTReverse.m
//  HTLibrary
//
//  Created by 최건우 on 2013. 11. 10..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "NSArray+HTReverse.h"

@implementation NSArray (HTReverse)

- (NSArray *)arrayByReversingObjects {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[self count]];
    NSEnumerator *enumerator = [self reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}

@end
