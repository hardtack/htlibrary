//
//  UIBarButtonItem+HTBackgroundImage.h
//
//  Created by 최건우 on 13. 6. 22..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (HTBackgroundImage)

@property (nonatomic, strong) UIButton *innerButton;

- (id)initWithBackgroundImage:(UIImage *)image leftMargin:(CGFloat)left target:(id)target action:(SEL)action;
- (id)initWithInnerButton:(UIButton *)button target:(id)target action:(SEL)action;

@end
