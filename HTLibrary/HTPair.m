//
//  HTPair.m
//
//  Created by 최건우 on 13. 6. 24..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTPair.h"

@implementation HTPair

#pragma mark - Class methods

+ (HTPair *)pairWithFirst:(id)first second:(id)second {
    return [[[self class] alloc] initWithFirst:first second:second];
}

#pragma mark - Initializers

- (id)initWithFirst:(id)first second:(id)second {
    self = [super init];
    if (self) {
        self.first = first;
        self.second = second;
    }
    return self;
}

#pragma mark - Public methods

- (BOOL)isEqual:(id)object {
    if ([object isKindOfClass:[HTPair class]]) {
        HTPair *other = object;
        return [self.first isEqual:other.first] && [self.second isEqual:other.second];
    }
    return [super isEqual:object];
}

@end
