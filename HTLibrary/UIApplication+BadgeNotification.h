//
//  UIApplication+BadgeNotification.h
//  meety_ios
//
//  Created by 최건우 on 13. 2. 20..
//  Copyright (c) 2013년 Meety. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const UIApplicationDidChangeApplicationIconBadgeNumber = @"kr.meety.UIApplicationDidChangeApplicationIconBadgeNumber";

@interface UIApplication (BadgeNotification)

@end
