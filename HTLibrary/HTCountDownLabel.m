//
//  HTCountDownLabel.m
//
//  Created by 최건우 on 13. 6. 28..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTCountDownLabel.h"

@implementation HTCountDownLabel

#pragma mark - Private methods

- (void)fire:(NSTimer *)timer {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDate *now = [NSDate date];
        if (self.formatter) {
            self.text = self.formatter(now, self.toDate);
        }
        [self.delegate countDownLabelDidFire:self];
    });
}

#pragma mark - Dealloc

- (void)dealloc {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - Public methods

- (void)setToDate:(NSDate *)toDate {
    _toDate = toDate;
    [self fire:self.timer];
}

- (void)setInterval:(NSTimeInterval)interval {
    _interval = interval;
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                  target:self
                                                selector:@selector(fire:)
                                                userInfo:nil
                                                 repeats:YES];
    [self fire:self.timer];
}

@end
