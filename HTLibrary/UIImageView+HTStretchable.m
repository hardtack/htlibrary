//
//  UIImageView+HTStretchable.m
//
//  Created by 최건우 on 13. 7. 1..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "UIImageView+HTStretchable.h"

@implementation UIImageView (HTStretchable)

- (void)makeImageStretchable {
    [self makeImageStretchableWithLeftCapWidth:self.image.size.width / 2 topCapHeight:self.image.size.height / 2];
}

- (void)makeImageStretchableWithLeftCapWidth:(NSInteger)leftCapWidth topCapHeight:(NSInteger)topCapHeight {
    self.image = [self.image stretchableImageWithLeftCapWidth:leftCapWidth topCapHeight:topCapHeight];
}

@end
