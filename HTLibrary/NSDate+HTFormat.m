//
//  NSDate+HTFormat.m
//
//  Created by 최건우 on 13. 7. 1..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "NSDate+HTFormat.h"

@implementation NSDate (HTFormat)

- (NSString *)descriptiveIntervalStringSinceDate:(NSDate *)sinceDate {
    NSTimeInterval interval = [self timeIntervalSinceDate:sinceDate];
    if (interval > 7 * 24 * 60 * 60) { // 7 Days ago
        return [NSDateFormatter localizedStringFromDate:sinceDate dateStyle:NSDateFormatterFullStyle timeStyle:NSDateFormatterNoStyle];
    }
    if (interval > 24 * 60 * 60) {
        NSUInteger component = (NSUInteger)interval / (24 * 60 * 60);
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"a day ago", @"a day ago")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d days ago", @"%d days ago"), component];
    }
    if (interval > 60 * 60) {
        NSUInteger component = (NSUInteger)interval / (60 * 60);
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"an hour ago", @"an hour ago")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d hours ago", @"%d hours ago"), component];
    }
    if (interval > 60) {
        NSUInteger component = (NSUInteger)interval / (60);
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"a minute ago", @"a minute ago")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d minutes ago", @"%d minutes ago"), component];
    }
    if (interval >= 0) {
        NSUInteger component = (NSUInteger)interval;
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"a second ago", @"a second ago")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d seconds ago", @"%d seconds ago"), component];
    }
    if (-interval < 60) {
        NSUInteger component = (NSUInteger)-interval;
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"a second after", @"a day after")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d seconds after", @"%d seconds after"), component];
    }
    if (-interval < 60 * 60) {
        NSUInteger component = (NSUInteger)-interval / (60);
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"a minute after", @"a minute after")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d minutes after", @"%d minutes after"), component];
    }
    if (-interval < 24 * 60 * 60) {
        NSUInteger component = (NSUInteger)-interval / (60 * 60);
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"an hour after", @"an hour after")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d hours after", @"%d days after"), component];
    }
    if (-interval < 7 * 24 * 60 * 60) {
        NSUInteger component = (NSUInteger)-interval / (24 * 60 * 60);
        if (component == 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"a day after", @"a day after")];
        }
        return [NSString stringWithFormat:NSLocalizedString(@"%d days after", @"%d days after"), component];
    }
    return [NSDateFormatter localizedStringFromDate:sinceDate dateStyle:NSDateFormatterFullStyle timeStyle:NSDateFormatterNoStyle];
}

- (NSString *)descriptiveIntervalStringSinceNow {
    return [self descriptiveIntervalStringSinceDate:[NSDate date]];
}

@end
