//
//  UIView+FrameAddition.m
//  iDC
//
//  Created by 건우 최 on 12. 1. 10..
//  Copyright (c) 2012년 Hardtack. All rights reserved.
//

#import "UIView+FrameAddition.h"

@implementation UIView (FrameAddition)
@dynamic origin;
@dynamic x;
@dynamic y;
@dynamic size;
@dynamic width;
@dynamic height;
@dynamic top;
@dynamic bottom;
@dynamic left;
@dynamic right;
@dynamic innerCenter;
@dynamic innerCenterX;
@dynamic innerCenterY;

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGFloat)x {
    return self.origin.x;
}

- (void)setX:(CGFloat)x {
    self.origin = CGPointMake(x, self.y);
}

- (CGFloat)y {
    return self.origin.y;
}

- (void)setY:(CGFloat)y {
    self.origin = CGPointMake(self.x, y);
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGFloat)width {
    return self.size.width;
}

- (void)setWidth:(CGFloat)width {
    self.size = CGSizeMake(width, self.height);
}

- (CGFloat)height {
    return self.size.height;
}

- (void)setHeight:(CGFloat)height {
    self.size = CGSizeMake(self.width, height);
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterX:(CGFloat)centerX {
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerY {
    return self.center.y;
}

- (void)setCenterY:(CGFloat)centerY {
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)top {
    return self.y;
}

- (void)setTop:(CGFloat)top {
    self.y = top;
}

- (CGFloat)bottom {
    return self.y + self.height;
}

- (void)setBottom:(CGFloat)bottom {
    self.y = bottom - self.height;
}

- (CGFloat)left {
    return self.x;
}

- (void)setLeft:(CGFloat)left {
    self.x = left;
}

- (CGFloat)right {
    return self.x + self.width;
}

- (void)setRight:(CGFloat)right {
    self.x = right - self.width;
}

- (CGPoint)innerCenter {
    return CGPointMake(self.innerCenterX, self.innerCenterY);
}

- (CGFloat)innerCenterX {
    return floorf(CGRectGetMaxX(self.bounds) / 2);
}

- (CGFloat)innerCenterY {
    return floorf(CGRectGetMaxY(self.bounds) / 2);
}

- (void)moveXToCenter {
    if ([self superview] == nil) {
        return;
    }
    self.center = CGPointMake([self superview].innerCenterX, self.center.y);
}

- (void)moveYToCenter {
    if ([self superview] == nil) {
        return;
    }
    self.center = CGPointMake(self.center.x, [self superview].innerCenterY);
}

- (void)moveToCenter {
    if ([self superview] == nil) {
        return;
    }
    self.center = [self superview].innerCenter;
}

@end

NSString* CGRectDescription(CGRect rect){
    return [NSString stringWithFormat:@"x : %f\n y : %f\n width : %f\n height : %f",
            rect.origin.x, rect.origin.y, rect.size.width, rect.size.height];
}
