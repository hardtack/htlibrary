//
//  NSURL+HTNormalization.m
//
//  Created by 최건우 on 13. 8. 7..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "NSURL+HTNormalization.h"

@implementation NSURL (HTNormalization)

- (NSURL *)normalizedURL {
    if (self.scheme != nil && self.host != nil) {
        return self;
    }
    
    NSString *URLString = [self absoluteString];
    if (self.scheme == nil) {
        if ([URLString hasPrefix:@"//"]) {
            URLString = [URLString substringFromIndex:[@"//" length]];
        }
        URLString = [NSString stringWithFormat:@"http://%@", URLString];
    }
    
    NSURL *URL = [NSURL URLWithString:URLString];
    if (URL.host == nil) {
        return nil;
    }
    return URL;
}

@end
