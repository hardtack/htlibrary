//
//  UILabel+HTSizeAdditions.m
//  PinkPouch
//
//  Created by 최건우 on 13. 6. 29..
//  Copyright (c) 2013년 Rolling Square. All rights reserved.
//

#import "UILabel+HTSizeAdditions.h"
#import "NSString+HTSizeAdditions.h"
#import "UIView+FrameAddition.h"

@implementation UILabel (HTSizeAdditions)

- (void)sizeToFitConstraintToWidth:(CGFloat)width {
    [self sizeToFitConstraintToSize:CGSizeMake(width, CGFLOAT_MAX)];
}

- (void)sizeToFitConstraintToSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = [self.text sizeWithLabel:self];
    if (self.numberOfLines != 1) {
        frame.size.height = MIN(size.height, frame.size.height);
    }
    self.frame = frame;
}

- (void)sizeToFitForFixedWidth {
    [self sizeToFitForFixedWidthWithMaxHeight:CGFLOAT_MAX];
}

- (void)sizeToFitForFixedWidthWithMaxHeight:(CGFloat)maxHeight {
    CGFloat width = self.frame.size.width;
    [self sizeToFitConstraintToSize:CGSizeMake(self.frame.size.width, maxHeight)];
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

@end
