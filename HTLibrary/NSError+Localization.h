//
//  NSError+Localization.h
//  meety_ios
//
//  Created by 최건우 on 12. 10. 27..
//  Copyright (c) 2012년 Meety. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Localization)

- (NSString *)localizedDomain;
- (NSString *)localizedAlertTitle;
- (NSString *)localizedAlertMessage;
- (UIAlertView *)localizedAlertView;
- (UIAlertView *)localizedAlertViewWithTitle:(NSString *)title;
- (UIAlertView *)localizedAlertViewWithDelegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitle, ... NS_REQUIRES_NIL_TERMINATION;
- (UIAlertView *)localizedAlertViewWithTitle:(NSString *)title delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitle, ... NS_REQUIRES_NIL_TERMINATION;

@end
