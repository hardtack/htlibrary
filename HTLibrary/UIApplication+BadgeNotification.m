//
//  UIApplication+BadgeNotification.m
//  meety_ios
//
//  Created by 최건우 on 13. 2. 20..
//  Copyright (c) 2013년 Meety. All rights reserved.
//

#import "UIApplication+BadgeNotification.h"
#import <objc/runtime.h>

@implementation UIApplication (BadgeNotification)
static Method setApplicationIconBadgeNumber = nil;

+ (void)initialize{
    if (!setApplicationIconBadgeNumber) {
        setApplicationIconBadgeNumber = class_getInstanceMethod(self, @selector(setApplicationIconBadgeNumber:));
        method_exchangeImplementations(setApplicationIconBadgeNumber, class_getInstanceMethod(self, @selector(notificationSetApplicationIconBadgeNumber:)));
    }
}

- (void)notificationSetApplicationIconBadgeNumber:(NSInteger)applicationIconBadgeNumber{
    [self notificationSetApplicationIconBadgeNumber:applicationIconBadgeNumber];
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidChangeApplicationIconBadgeNumber object:self];
}

@end
