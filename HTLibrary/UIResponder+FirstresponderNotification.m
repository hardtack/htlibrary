//
//  UIResponder+FirstresponderNotification.m
//  meety_ios
//
//  Created by 최건우 on 12. 10. 13..
//  Copyright (c) 2012년 Meety. All rights reserved.
//

#import "UIResponder+FirstresponderNotification.h"
#import <objc/runtime.h>

@implementation UIResponder (FirstresponderNotification)

static Method becomeFirstResponderMethod = nil;

- (BOOL)becomeFirstResponder_{
    BOOL result = [self becomeFirstResponder_];
    if (result) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UIResponderDidChangeFirstResponderNotification object:self];
    }
    return result;
}

+ (void)initialize {
    if(!becomeFirstResponderMethod) {
        becomeFirstResponderMethod = class_getInstanceMethod(self, @selector(becomeFirstResponder));
        method_exchangeImplementations(becomeFirstResponderMethod,
                                       class_getInstanceMethod(self, @selector(becomeFirstResponder_)));
    }
}

@end
