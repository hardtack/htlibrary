//
//  UIResponder+FirstresponderNotification.h
//  meety_ios
//
//  Created by 최건우 on 12. 10. 13..
//  Copyright (c) 2012년 Meety. All rights reserved.
//

#import <UIKit/UIKit.h>
static NSString *const UIResponderDidChangeFirstResponderNotification = @"me.hardtack.UIResponderDidChangeFirstResponderNotification";

@interface UIResponder (FirstresponderNotification)

@end
