//
//  NSMutableArray+HTReverse.m
//  HTLibrary
//
//  Created by 최건우 on 2013. 11. 10..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "NSMutableArray+HTReverse.h"

@implementation NSMutableArray (HTReverse)

- (void)reverseObjects {
    if ([self count] == 0)
        return;
    NSUInteger i = 0;
    NSUInteger j = [self count] - 1;
    while (i < j) {
        [self exchangeObjectAtIndex:i
                  withObjectAtIndex:j];
        
        i++;
        j--;
    }
}

@end
