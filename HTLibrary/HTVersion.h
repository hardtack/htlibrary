//
//  HTVersion.h
//
//  Created by 최건우 on 13. 7. 25..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTVersion : NSObject

@property (strong, nonatomic) NSString *string;

+ (instancetype)versionWithNumber:(NSNumber *)number;
+ (instancetype)versionWithString:(NSString *)string;

- (id)initWithString:(NSString *)string;

// Default implementation is not reliable. You must override it.
- (NSComparisonResult)compare:(HTVersion *)version;

@end

NSComparisonResult NSUIntegerCompare(NSUInteger left, NSUInteger right);
NSComparisonResult NSIntegerCompare(NSInteger left, NSInteger right);
