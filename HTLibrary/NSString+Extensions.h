//
//  NSStringExtensions.h
//  iDC
//
//  Created by 건우 최 on 11. 12. 31..
//  Copyright (c) 2011년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringExtensions)

@property (nonatomic, readonly) NSUInteger lastIndex;

- (NSString *)xmlSimpleUnescapedString;
- (NSString *)xmlSimpleEscapedString;
- (NSString *) stringByReplacingHtmlTagWithString:(NSString*)string;
- (NSString *) stringByEscapseXml;
- (NSString *) stringByUnescapeXml;
- (NSRange) rangeOfString:(NSString*)aString withRange:(NSRange)range;
- (NSRange) rangeOfStringForTail:(NSString*)str;
- (NSRange) rangeOfStringForTail:(NSString*)str withLocation:(NSUInteger)location;
- (NSString *)trimmedString;

@end
