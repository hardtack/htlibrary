//
//  HTCGRectFunctions.h
//  HTLibrary
//
//  Created by 최건우 on 13. 9. 1..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <UIKit/UIKit.h>

CGRect CGRectApplyPadding(CGRect rect, UIEdgeInsets padding);
