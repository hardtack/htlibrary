//
//  HTNextTextFieldDelegate.h
//
//  Created by 최건우 on 13. 6. 24..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTNextTextFieldDelegate : NSObject <UITextFieldDelegate>

@property (strong, nonatomic) NSArray *fields;

+ (HTNextTextFieldDelegate *)nextDelegate;
+ (HTNextTextFieldDelegate *)nextDelegateWithFields:(NSArray *)fields;

@end
