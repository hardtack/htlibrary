//
//  HTCGSizeFunctions.h
//  meety_ios
//
//  Created by 최건우 on 12. 10. 27..
//

#import <Foundation/Foundation.h>

CGSize CGSizeResizeToFit(CGSize size, CGSize toFit);
CGSize CGSizeResizeWithWidth(CGSize size, CGFloat width);
CGSize CGSizeResizeWithHeight(CGSize size, CGFloat height);