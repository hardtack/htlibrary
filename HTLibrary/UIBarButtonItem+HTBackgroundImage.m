//
//  UIBarButtonItem+HTBackgroundImage.m
//
//  Created by 최건우 on 13. 6. 22..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <objc/runtime.h>
#import "UIBarButtonItem+HTBackgroundImage.h"
#import "UIImage+SizeAddition.h"

@implementation UIBarButtonItem (HTBackgroundImage)
@dynamic innerButton;

#pragma mark - Private methods

- (void)innerButtonPressed:(id)sender {
    if (self.target != nil && self.action != nil) {
        NSMethodSignature *signature = [[self.target class] instanceMethodSignatureForSelector:self.action];
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        [invocation setTarget:self.target];
        [invocation setSelector:self.action];
        if (signature.numberOfArguments > 2){
            [invocation setArgument:(void *)&self atIndex:2];
        }
        [invocation retainArguments];
        [invocation invoke];
    }
}

#pragma mark - Initializers

- (id)initWithInnerButton:(UIButton *)button target:(id)target action:(SEL)action {
    self = [self initWithCustomView:button];
    if (self) {
        self.target = target;
        self.action = action;
        self.innerButton = button;
        [self.innerButton addTarget:self action:@selector(innerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (id)initWithBackgroundImage:(UIImage *)image leftMargin:(CGFloat)left target:(id)target action:(SEL)action {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.width + left * 2, image.height)];
    button.contentMode = UIViewContentModeCenter;
    [button setImage:image forState:UIControlStateNormal];
    return [self initWithInnerButton:button target:target action:action];
}

#pragma mark - Getters and Setters

- (void)setInnerButton:(UIButton *)innerButton {
    objc_setAssociatedObject(self, "innerButton", innerButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIButton *)innerButton {
    return objc_getAssociatedObject(self, "innerButton");
}

@end
