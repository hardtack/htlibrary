//
//  HTSemanticVersion.m
//
//  Created by 최건우 on 13. 7. 25..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTSemanticVersion.h"

@implementation HTSemanticVersion

#pragma mark - Initializers

- (id)initWithString:(NSString *)string {
    self = [super initWithString:string];
    if (self) {
        NSArray *components = [string componentsSeparatedByString:@"."];
        if ([components count] > 3) { // Invalid string
            return nil;
        }
        NSInteger versions[3] = {0,};
        NSUInteger i = 0;
        for (NSString *component in components) {
            NSScanner *scanner = [NSScanner scannerWithString:component];
            NSInteger v;
            if (![scanner scanInteger:&v]) {
                return nil;
            }
            versions[i] = v;
            i++;
        }
        self.majorVersion = versions[0];
        self.minorVersion = versions[1];
        self.patchVersion = versions[2];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@.%@.%@", @(self.majorVersion), @(self.minorVersion), @(self.patchVersion)];
}

- (NSComparisonResult)compare:(HTVersion *)version {
    if (![version isKindOfClass:[self class]]) {
        return [super compare:version];
    }
    HTSemanticVersion *v = (HTSemanticVersion *)version;
    NSComparisonResult r = NSIntegerCompare(self.majorVersion, v.majorVersion);
    if (r == NSOrderedSame) {
        r = NSIntegerCompare(self.minorVersion, v.minorVersion);
    }
    if (r == NSOrderedSame) {
        r = NSIntegerCompare(self.patchVersion, v.patchVersion);
    }
    return r;
}

@end
