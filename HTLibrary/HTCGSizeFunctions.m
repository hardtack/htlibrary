//
//  HTCGSizeFunctions.m
//  meety_ios
//
//  Created by 최건우 on 12. 10. 27..
//

#import "HTCGSizeFunctions.h"

CGSize CGSizeResizeWithWidth(CGSize size, CGFloat width){
    if (size.width == 0){
        return CGSizeMake(0, size.height);
    }
    CGFloat p = width / size.width;
    size.width = width;
    size.height = floorf(size.height * p);
    return size;
}

CGSize CGSizeResizeWithHeight(CGSize size, CGFloat height){
    if (size.height == 0){
        return CGSizeMake(size.width, 0);
    }
    CGFloat p = height / size.height;
    size.height = height;
    size.width = floorf(size.width * p);
    return size;
}

CGSize CGSizeResizeToFit(CGSize size, CGSize toFit){
    size = CGSizeResizeWithWidth(size, toFit.width);
    if (size.height > toFit.height) {
        size = CGSizeResizeWithHeight(size, toFit.height);
    }
    return size;
}