//
//  BENextTextFieldDelegate.m
//
//  Created by 최건우 on 13. 6. 24..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTNextTextFieldDelegate.h"

@implementation HTNextTextFieldDelegate

+ (HTNextTextFieldDelegate *)nextDelegate {
    return [[[self class] alloc] init];
}

+ (HTNextTextFieldDelegate *)nextDelegateWithFields:(NSArray *)fields {
    HTNextTextFieldDelegate *instance = [self nextDelegate];
    instance.fields = fields;
    return instance;
}

- (void)setFields:(NSArray *)fields {
    _fields = fields;
    for (UITextField *field in fields) {
        field.delegate = self;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSUInteger idx = [self.fields indexOfObject:textField];
    if (idx + 1 < self.fields.count) {
        [self.fields[idx + 1] becomeFirstResponder];
    }
    return YES;
}

@end
