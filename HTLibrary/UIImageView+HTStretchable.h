//
//  UIImageView+HTStretchable.h
//
//  Created by 최건우 on 13. 7. 1..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (HTStretchable)

- (void)makeImageStretchable;
- (void)makeImageStretchableWithLeftCapWidth:(NSInteger)leftCapWidth topCapHeight:(NSInteger)topCapHeight;

@end
