//
//  HTSemanticVersion.h
//
//  Created by 최건우 on 13. 7. 25..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTVersion.h"

@interface HTSemanticVersion : HTVersion

@property (nonatomic) NSInteger majorVersion;
@property (nonatomic) NSInteger minorVersion;
@property (nonatomic) NSInteger patchVersion;

@end
