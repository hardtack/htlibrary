//
//  UIView+FrameAddition.h
//  iDC
//
//  Created by 건우 최 on 12. 1. 10..
//  Copyright (c) 2012년 Hardtack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FrameAddition)

/* Basic frame properties */
@property (nonatomic) CGPoint origin;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGSize size;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat centerX;
@property (nonatomic) CGFloat centerY;

/* Additional properties */
@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat bottom;
@property (nonatomic) CGFloat left;
@property (nonatomic) CGFloat right;
@property (nonatomic, readonly) CGPoint innerCenter;
@property (nonatomic, readonly) CGFloat innerCenterX;
@property (nonatomic, readonly) CGFloat innerCenterY;

/* Moving */
- (void)moveXToCenter;
- (void)moveYToCenter;
- (void)moveToCenter;

@end

NSString* CGRectDescription(CGRect rect);
