//
//  HTFile.m
//
//  Created by 최건우 on 13. 6. 16..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTFile.h"

@implementation HTFile

- (id)initWithData:(NSData *)data name:(NSString *)name mimeType:(NSString *)mimeType {
    self = [super init];
    if (self) {
        self.data = data;
        self.name = name;
        self.mimeType = mimeType;
    }
    return self;
}

+ (HTFile *)fileWithData:(NSData *)data name:(NSString *)name mimeType:(NSString *)mimeType {
    return [[[self class] alloc] initWithData:data name:name mimeType:mimeType];
}

@end
