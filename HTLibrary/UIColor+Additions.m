//
//  UIColor+Additions.m
//  YoureMyPet
//
//  Created by 건우 최 on 12. 1. 11..
//  Copyright (c) 2012년 Hardtack. All rights reserved.
//

#import "UIColor+Additions.h"
#define logB(x, base) (log((x))/ log((base)))
#define hexlen(hex) ((NSInteger)(logB((hex), 16) + 1))

@implementation UIColor (Additions)

+ (UIColor*)colorWithRGBHex:(NSInteger)hex alpha:(CGFloat)alpha{
    NSInteger blue = hex % 256;
    hex /= 256;
    NSInteger green = hex % 256;
    hex /= 256;
    NSInteger red = hex % 256;
    return [UIColor colorWithIntegerRed:red green:green blue:blue alpha:alpha];
}

+ (UIColor*)colorWithIntegerRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha{
    CGFloat colorMax = 255.0f;
    return [UIColor colorWithRed:red / colorMax green:green / colorMax blue:blue / colorMax alpha:alpha];
}
@end
