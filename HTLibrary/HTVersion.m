//
//  HTVersion.m
//
//  Created by 최건우 on 13. 7. 25..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import "HTVersion.h"

@implementation HTVersion

#pragma mark - Private methods

#pragma mark - Class methods

+ (instancetype)versionWithString:(NSString *)string {
    return [[[self class] alloc] initWithString:string];
}

+ (instancetype)versionWithNumber:(NSNumber *)number {
    return [self versionWithString:[number description]];
}

#pragma mark - Initializers

- (id)init {
    return [self initWithString:nil];
}

- (id)initWithString:(NSString *)string {
    self = [super init];
    if (self) {
        self.string = string;
    }
    return self;
}

#pragma mark - Public methods

- (NSComparisonResult)compare:(HTVersion *)version {
    return [self.string isEqualToString:version.string] ? NSOrderedSame : NSOrderedDescending;
}

@end

NSComparisonResult NSUIntegerCompare(NSUInteger left, NSUInteger right) {
    if (left < right) {
        return NSOrderedAscending;
    } else if (left > right) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

NSComparisonResult NSIntegerCompare(NSInteger left, NSInteger right) {
    if (left < right) {
        return NSOrderedAscending;
    } else if (left > right) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}