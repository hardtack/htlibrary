//
//  NSError+Localization.m
//  meety_ios
//
//  Created by 최건우 on 12. 10. 27..
//  Copyright (c) 2012년 Meety. All rights reserved.
//

#import "NSError+Localization.h"

@implementation NSError (Localization)

- (NSString*)localizedDomain
{
    return NSLocalizedString(self.domain, @"Domain");
}

- (NSString*)localizedAlertTitle
{
    return [[NSString stringWithFormat:@"%@ %@ (%@)", [self localizedDomain], NSLocalizedString(@"Error", @"Error"), @(self.code)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (UIAlertView *)localizedAlertView
{
    return [self localizedAlertViewWithTitle:[self localizedAlertTitle]];
}

- (UIAlertView *)localizedAlertViewWithTitle:(NSString *)title
{
    return [self localizedAlertViewWithTitle:title
                                    delegate:nil
                           cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss")
                           otherButtonTitles:nil];
}

- (NSString *)localizedAlertMessage
{
    NSString* msg = [self localizedFailureReason];
    if ([msg length] == 0) {
        msg = [self localizedDescription];
    }
    return msg;
}

- (UIAlertView*)localizedAlertViewWithDelegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitle, ...
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:[self localizedAlertTitle]
                                                        message:[self localizedAlertMessage]
                                                       delegate:delegate
                                              cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    va_list args;
    va_start(args, otherButtonTitle);
    for (NSString* str=otherButtonTitle; str != nil; str=va_arg(args, NSString*)){
        [alertView addButtonWithTitle:str];
    }
    va_end(args);
    return alertView;
}


- (UIAlertView *)localizedAlertViewWithTitle:(NSString *)title delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitle, ... NS_REQUIRES_NIL_TERMINATION
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:[self localizedAlertMessage]
                                                       delegate:delegate
                                              cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    va_list args;
    va_start(args, otherButtonTitle);
    for (NSString* str=otherButtonTitle; str != nil; str=va_arg(args, NSString*)){
        [alertView addButtonWithTitle:str];
    }
    va_end(args);
    return alertView;
}

@end
