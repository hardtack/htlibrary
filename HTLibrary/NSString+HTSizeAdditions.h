//
//  NSString+HTSizeAdditions.h
//  PinkPouch
//
//  Created by 최건우 on 13. 6. 29..
//  Copyright (c) 2013년 Rolling Square. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTSizeAdditions)

- (CGSize)sizeWithLabel:(UILabel *)label;

@end
