//
//  UILabel+HTSizeAdditions.h
//  PinkPouch
//
//  Created by 최건우 on 13. 6. 29..
//  Copyright (c) 2013년 Rolling Square. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HTSizeAdditions)

- (void)sizeToFitConstraintToWidth:(CGFloat)width;
- (void)sizeToFitConstraintToSize:(CGSize)size;
- (void)sizeToFitForFixedWidth;
- (void)sizeToFitForFixedWidthWithMaxHeight:(CGFloat)maxHeight;

@end
