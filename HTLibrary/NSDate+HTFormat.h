//
//  NSDate+HTFormat.h
//
//  Created by 최건우 on 13. 7. 1..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (HTFormat)

- (NSString *)descriptiveIntervalStringSinceDate:(NSDate *)sinceDate;
- (NSString *)descriptiveIntervalStringSinceNow;

@end
