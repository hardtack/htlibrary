//
//  NSString+NumberAddition.m
//
//  Created by 건우 최 on 12. 1. 12..
//

#import "NSString+NumberAddition.h"

@implementation NSString(NumberAddition)

+ (NSString*)stringWithInt:(int)val{
    return [NSString stringWithFormat:@"%d", val];
}

+ (NSString*)stringWithInteger:(NSInteger)val{
    return [NSString stringWithFormat:@"%@", @(val)];
}

+ (NSString*)stringWithNumber:(NSNumber*)val{
    return [NSString stringWithFormat:@"%@", val];
}
+ (NSString*)stringWithFloat:(float)val{
    return [NSString stringWithFormat:@"%f", val];
}

+ (NSString*)stringWithDouble:(double)val{
    return [NSString stringWithFormat:@"%f", val];
}

+ (NSString*)stringWithUniChar:(UniChar)val{
    return [NSString stringWithFormat:@"%c", val];
}

@end