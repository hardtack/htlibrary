//
//  HTPair.h
//
//  Created by 최건우 on 13. 6. 24..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTPair : NSObject

@property (nonatomic, strong) id first;
@property (nonatomic, strong) id second;

- (id)initWithFirst:(id)first second:(id)second;
+ (HTPair *)pairWithFirst:(id)first second:(id)second;

@end
