//
//  NSURL+HTNormalization.h
//
//  Created by 최건우 on 13. 8. 7..
//  Copyright (c) 2013년 Hardtack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (HTNormalization)
/**
 * Normalizes URL.  If URL is malformed, then treat it as http URL.
 * Returns `nil` if it fails to normalize URL.
 */
- (NSURL *)normalizedURL;

@end
